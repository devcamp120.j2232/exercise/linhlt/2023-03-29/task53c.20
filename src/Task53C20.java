import model.Movablepoint;

public class Task53C20 {
    public static void main(String[] args) throws Exception {
        Movablepoint point1 = new Movablepoint(5, 6);
        Movablepoint point2 = new Movablepoint(9, 2);
        System.out.println("Original point 1: " + point1);
        System.out.println("Original point 2: " + point2);
        point1.moveUp();
        point1.moveUp();
        point1.moveLeft();
        point2.moveDown();
        point2.moveRight();
        point2.moveRight();
        System.out.println("New point 1: " + point1);
        System.out.println("New point 2: " + point2);
    }
}
